# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import SUPERUSER_ID
from odoo import fields, models, api


class res_users(models.Model):
    _inherit = 'res.users'
    sale_team_id = fields.Many2one('crm.team','Sales Team')



class res_partner(models.Model):
    _inherit = 'res.partner'

    def _default_team(self):
        res = False
        ctx = self.env.context
        if ctx.get('search_default_customer') or ctx.get('default_customer'):
            team = self.env['res.users'].sudo().browse(self._uid).sale_team_id
            res = team and team.id
        return res

    @api.model
    def _default_user(self):
        res = False
        ctx = self.env.context
        if ctx.get('search_default_customer') or ctx.get('default_customer'):
            res = self._uid
        return res

    team_id = fields.Many2one('crm.team', 'Sales Team', oldname='section_id', default=_default_team)
    user_id = fields.Many2one('res.users', string='Salesperson',  default=_default_user )

