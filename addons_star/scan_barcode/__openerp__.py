# -*- encoding: utf-8 -*-
###########################################################################
#    Copyright (C) 2016 - Turkesh Patel. <http://turkeshpatel.odoo.com>
#
#    @author Turkesh Patel <turkesh4friends@gmail.com>
###########################################################################

{
    'name': 'Add Products by Barcode in Sale, Purchase and Invoice',
    'version': '1.0',
    'category': 'Accounting',
    'author': 'Turkesh Patel',
    'summary': """Add Products by scanning barcode to avoid mistakes and make work faster in Sale, Purchase and Invoice.""",
    'description': """Add Products by scanning barcode to avoid mistakes and make work faster in Sale, Purchase and Invoice.""",
    'website': 'https://turkeshpatel.odoo.com', 
    "depends": ['account','sale','purchase'],
    "data": [
        "views/sale_view.xml",
        "views/purchase_view.xml",
        "views/account_invoice_view.xml",
    ],
    'images': [
        'static/description/barcode_cover.jpg',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
    'price': 32,
    'currency': 'EUR',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
