# -*- encoding: utf-8 -*-

from openerp import api, fields, models, SUPERUSER_ID, _


class purchase_order(models.Model):
    _inherit = "purchase.order"

    get_product_ean = fields.Char(string='Add Product', size=13, help="Read a product barcode to add it as new Line.")

    @api.onchange('get_product_ean')
    def onchange_product_ean(self):

        if not self.get_product_ean:
            return

        ProductObj = self.env['product.product']
        product = ProductObj.search([('barcode','=',self.get_product_ean)], limit=1)
        if not product:
            product = ProductObj.search([('default_code','=',self.get_product_ean)], limit=1)
        if not product:
            warning = {'title': _('Warning!'),
                'message': _("There is no product with Barcode or Reference: %s") % (self.get_product_ean),
            }
            self.get_product_ean = False
            return {'warning': warning}

        flag = 1
        order_line = []
        for o_line in self.order_line:
            if o_line.product_id == product:
                flag = 0
                order_line.append((0,0,{
                    'product_id': o_line.product_id.id,
                    'name': o_line.product_id.name,
                    'taxes_id': o_line.taxes_id,
                    'product_qty' : (o_line.product_qty + 1),
                    'price_unit': o_line.price_unit,
                    'product_uom': o_line.product_uom and o_line.product_uom.id,
                    'state' : 'draft',
                }))
            else:
                order_line.append((0,0,{
                    'product_id': o_line.product_id.id,
                    'name': o_line.product_id.name,
                    'taxes_id': o_line.taxes_id,
                    'product_qty' : o_line.product_qty,
                    'price_unit': o_line.price_unit,
                    'product_uom': o_line.product_uom and o_line.product_uom.id,
                    'state' : 'draft',
                }))
        if flag:
            order_line.append((0,0, {
            'product_id': product.id,
            'name': product.name,
            'taxes_id': [(6, 0, product.supplier_taxes_id.ids)],
            'product_qty' : 1,
            'price_unit': product.standard_price,
            'product_uom': product.uom_po_id.id ,
            'state' : 'draft',
        }))
        self.get_product_ean = False
        self.order_line = order_line


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: 
