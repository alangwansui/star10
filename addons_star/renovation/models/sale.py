# -*- encoding: utf-8 -*-
##############################################################################
from odoo.tools.translate import _
from odoo import api, fields, models, _


# class product_product(models.Model):
#    _inherit = 'product.product'
#
#    uom2_id = fields.Many2one('product.uom', u'第二单位')
#    uom2_rate = fields.Float(u'比率')

class res_partner(models.Model):
   _inherit = 'res.partner'

   def get_address_oneline(self):
      self.ensure_one()
      N = ''
      country = self.country_id.name or N
      city = self.city or N
      street = self.street or N
      street2 = self.street2 or N
      return  country + city + street + street2



class sale_order(models.Model):
   _inherit = 'sale.order'

   designer_uid = fields.Many2one('res.users', u'设计师')


class sale_order_line(models.Model):
   _inherit = 'sale.order.line'


   @api.onchange('long', 'width', 'quantum')
   def onchange_quantum(self):
      if any( [self.long , self.width , self.quantum] ):
         qty = (self.long or 1) * (self.width or 1) * (self.quantum or 1)
         self.product_uom_qty = qty

   long = fields.Float(u'长')
   width = fields.Float(u'宽')
   quantum = fields.Integer(u'份')


class calendar_event(models.Model):
   _inherit = 'calendar.event'

   _defaults = {
      'class': 'private',
   }




















#############################################################################################
