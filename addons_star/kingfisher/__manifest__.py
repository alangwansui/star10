# -*- coding: utf-8 -*-
# Part of AppJetty. See LICENSE file for full copyright and licensing details.

{
    'name': 'Kingfisher Theme',
    'description': 'Kingfisher Theme',
    'category': 'Theme/Ecommerce',
    'version': '10.0.1.0.0',
    'author': 'AppJetty',
    'website': 'https://www.biztechconsultancy.com',
    'depends': [
        'website_sale',
        'mass_mailing',
        'website_blog',
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/view.xml',
        'views/theme.xml',
        'views/slider_view.xml',
        'views/snippets.xml',
        'views/theme_customize.xml',
    ],
    'application': True,
    'live_test_url': 'http://theme-kingfisher-v10.appjetty.com',
    'images': ['static/description/splash-screen.png'],
    'price': 99.00,
    'currency': 'EUR',
}
