# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Menu Active',
    'version': '1.0',
    'summary': 'You can set the menu to hide',
    'category': 'tools',
    'author': '<jon alangwansui@gmail.com>',
    'data': [
        'menu.xml'
    ],
    'depends' : ['base',],
    'price': 20,
    'installable': True,
}


