# -*- encoding: utf-8 -*-
##############################################################################
from openerp.tools.translate import _
from openerp import api, fields, models, _



class ir_translation(models.Model):
    _inherit = 'ir.translation'

    def translate_menu_by_xml(self, cr, uid,  xml_id, source, value):
        data_obj = self.pool.get("ir.model.data")
        menu_id = data_obj.xmlid_to_res_id(cr, uid, xml_id)
        if menu_id:
            trans_ids = self.search(cr, uid, [('name', '=', 'ir.ui.menu,name'), ('lang', '=', 'zh_CN'),('res_id', '=', menu_id)])
            if trans_ids:
                self.write(cr, uid, trans_ids, {'value': value})
            else:
                module = xml_id.split('.')[0]
                self.create(cr, uid,
                            {'name': 'ir.ui.menu,name',
                             'res_id': menu_id,
                             'lang': 'zh_CN',
                             'module': module,
                             'type': 'model',
                             'source': source,
                             'value': value})

